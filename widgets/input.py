import os
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivymd.textfields import MDTextField
from kivy.clock import Clock
from kivy.core.window import Window

from widgets.dialog import InputDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'input.kv'))


class SensorMDTextField(MDTextField):
    ignore_clear = BooleanProperty(False)
    ignore_validate = BooleanProperty(False)
    use_dlg = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(SensorMDTextField, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_dlg)

    def bind_dlg(self, *args):
        if self.use_dlg:
            self.bind(focus=self.show_dlg)

    def show_dlg(self, inst, val):
        if val and not self.disabled:
            Window.release_all_keyboards()
            dlg = InputDialog(text=self.text, hint_text=self.hint_text, input_filter=self.input_filter)
            dlg.bind(on_confirm=self.on_confirm)
            dlg.open()

    def on_confirm(self, *args):
        self.text = args[1]
        self.dispatch('on_text_validate')


class NumericTextField(BoxLayout):

    hint_text = StringProperty('')
    value = NumericProperty(0)
    default_value = NumericProperty(0)
    icon_size = NumericProperty(30)
    minimum = NumericProperty(-1000000)
    maximum = NumericProperty(1000000)

    def __init__(self, **kwargs):
        super(NumericTextField, self).__init__(**kwargs)
        if 'value' in kwargs.keys():
            self.set_value(kwargs['value'])
        Clock.schedule_once(lambda dt: self.bind_event())

    def bind_event(self):
        self.ids.input.bind(on_text_validate=self.on_text_validate)

    def on_plus(self):
        value = int(self.ids.input.text)
        if value < self.maximum:
            self.value = value + 1
            self.ids.input.text = str(self.value)

    def on_minus(self):
        value = int(self.ids.input.text)
        if value > self.minimum:
            self.value = value - 1
            self.ids.input.text = str(self.value)

    def set_value(self, val):
        try:
            val = int(val)
            val = min(val, self.maximum)
            val = max(val, self.minimum)
            self.value = val
            self.ids.input.text = str(val)
        except ValueError:
            raise ValueError('Error, must be numeric')

    def on_text_validate(self, *args):
        self.set_value(args[0].text)

    def get_value(self):
        return int(self.ids.input.text)

    def clear(self):
        self.set_value(self.default_value)

    def on_value(self, *args):
        self.set_value(args[1])

    def on_icon_size(self, *args):
        self.icon_size = args[1]
