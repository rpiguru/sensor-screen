from kivy.factory import Factory
from kivymd.button import MDFlatButton, MDFloatingActionButton, MDIconButton, MDRaisedButton
from kivymd.toolbar import Toolbar
from kivymd.theming import ThemeManager
from kivymd.navigationdrawer import MDNavigationDrawer
from kivymd.navigationdrawer import NavigationLayout
from kivymd.navigationdrawer import NavigationDrawerDivider
from kivymd.navigationdrawer import NavigationDrawerToolbar
from kivymd.navigationdrawer import NavigationDrawerSubheader
from kivymd.selectioncontrols import MDCheckbox
from kivymd.selectioncontrols import MDSwitch
from kivymd.label import MDLabel
from kivymd.list import MDList
from kivymd.list import OneLineListItem
from kivymd.list import TwoLineListItem
from kivymd.list import ThreeLineListItem
from kivymd.list import OneLineAvatarListItem
from kivymd.list import OneLineIconListItem
from kivymd.list import OneLineAvatarIconListItem
from kivymd.textfields import MDTextField
from kivymd.spinner import MDSpinner
from kivymd.card import MDCard
from kivymd.card import MDSeparator
from kivymd.menu import MDDropdownMenu
from kivymd.grid import SmartTile
from kivymd.slider import MDSlider
from kivymd.tabs import MDTabbedPanel
from kivymd.tabs import MDTab
from kivymd.progressbar import MDProgressBar
from kivymd.accordion import MDAccordion
from kivymd.accordion import MDAccordionItem
from kivymd.accordion import MDAccordionSubItem
from kivymd.theme_picker import MDThemePicker
from kivymd.tabs import MDBottomNavigation
from kivymd.tabs import MDBottomNavigationItem
from widgets.input import NumericTextField
from widgets.label import ScrollableLabel
from widgets.sensor_item import SensorItem

Factory.register("Toolbar", cls=Toolbar)
Factory.register('ThemeManager', cls=ThemeManager)
Factory.register('MDFlatButton', cls=MDFlatButton)
Factory.register('MDFloatingActionButton', cls=MDFloatingActionButton)
Factory.register('MDIconButton', cls=MDIconButton)
Factory.register('MDRaisedButton', cls=MDRaisedButton)
Factory.register('MDNavigationDrawer', cls=MDNavigationDrawer)
Factory.register('NavigationLayout', cls=NavigationLayout)
Factory.register('NavigationDrawerDivider', cls=NavigationDrawerDivider)
Factory.register('NavigationDrawerToolbar', cls=NavigationDrawerToolbar)
Factory.register('NavigationDrawerSubheader', cls=NavigationDrawerSubheader)
Factory.register('MDCheckbox', cls=MDCheckbox)
Factory.register('MDSwitch', cls=MDSwitch)
Factory.register('MDLabel', cls=MDLabel)
Factory.register('MDList', cls=MDList)
Factory.register('OneLineListItem', cls=OneLineListItem)
Factory.register('TwoLineListItem', cls=TwoLineListItem)
Factory.register('ThreeLineListItem', cls=ThreeLineListItem)
Factory.register('OneLineAvatarListItem', cls=OneLineAvatarListItem)
Factory.register('OneLineIconListItem', cls=OneLineIconListItem)
Factory.register('OneLineAvatarIconListItem', cls=OneLineAvatarIconListItem)
Factory.register('MDTextField', cls=MDTextField)
Factory.register('MDSpinner', cls=MDSpinner)
Factory.register('MDCard', cls=MDCard)
Factory.register('MDSeparator', cls=MDSeparator)
Factory.register('MDDropdownMenu', cls=MDDropdownMenu)
Factory.register('SmartTile', cls=SmartTile)
Factory.register('MDSlider', cls=MDSlider)
Factory.register('MDTabbedPanel', cls=MDTabbedPanel)
Factory.register('MDTab', cls=MDTab)
Factory.register('MDProgressBar', cls=MDProgressBar)
Factory.register('MDAccordion', cls=MDAccordion)
Factory.register('MDAccordionItem', cls=MDAccordionItem)
Factory.register('MDAccordionSubItem', cls=MDAccordionSubItem)
Factory.register('MDThemePicker', cls=MDThemePicker)
Factory.register('MDBottomNavigation', cls=MDBottomNavigation)
Factory.register('MDBottomNavigationItem', cls=MDBottomNavigationItem)

# Custom widgets
Factory.register('SensorItem', cls=SensorItem)
Factory.register('NumericTextField', cls=NumericTextField)
Factory.register('ScrollableLable', cls=ScrollableLabel)
