import os
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'sensor_item.kv'))


class SensorItem(BoxLayout):
    label = StringProperty()
    value = NumericProperty()
    unit = StringProperty()

    def get_value(self):
        return self.value

    def set_value(self, val):
        self.value = val
