# Touchscreen Application running on RPi for the Coffee Grinder

## Components

- Raspberry Pi 3
    
    https://www.raspberrypi.org/products/raspberry-pi-3-model-b/
   
- Raspberry Pi Official 7" Touchscreen
    
    https://www.raspberrypi.org/products/raspberry-pi-touch-display/

    https://thepihut.com/blogs/raspberry-pi-tutorials/45295044-raspberry-pi-7-touch-screen-assembly-guide

- BME280 sensor
    
    https://www.adafruit.com/product/2652

## Connection


![BreadBoard View](schematic/schematic_bb.jpg)

![Schematic View](schematic/schematic_schem.jpg)


## Installation

- Download the latest Raspbian **Stretch Lite** from [here](https://www.raspberrypi.org/downloads/raspbian/) and flash your RPi.

    https://www.raspberrypi.org/documentation/installation/installing-images/

- After flashing the *Raspbian Stretch Lite*, turn RPi on and clone this repository on the terminal:
    
        cd ~
        sudo apt-get install -y git
        git clone https://gitlab.com/blocksimple/coffee-bot/sensor-screen
        
- Install everything:
        
        cd ~/sensor-screen
        sudo bash install.sh

- Now, reboot!

## How to run the application manually
    
    cd /home/pi/sensor-screen
    sudo python3 main.py

## Key Code Packages

- Kivy

    https://kivy.org/#home

- Peewee

    https://github.com/coleifer/peewee

- KivyMD
    
    https://gitlab.com/kivymd/KivyMD
    
- Fritzing
    
    http://fritzing.org/home/

- Future hardware for the Mini Pi
    
    * Raspberry Pi Zero W
    
    * 3.5" touchscreen from Waveshare
        
        https://www.waveshare.com/product/3.5inch-RPi-LCD-B.htm
    
    * BME280 from ebay
        
        https://www.ebay.com/itm/Digital-Barometric-Pressure-Sensor-Board-Swap-I2C-SPI-BMP280-BME280-3-3V-CA/142512111833?hash=item212e6240d9:g:yWcAAOSwrlRZwMRs
    
## WiFi Setup
    
    sudo raspi-config

And go to `2 Network Options` => `N2 Wi-fi` and input **SSID** & **passphrase** and reboot.


## How to use *dataplicity* to allow remote access

1. Visit www.dataplicity.com and sign up.

2. They will give you a script something like:
    
        curl https://www.dataplicity.com/xxxxxxxx.py | sudo python
    
3. Now, execute this script on the terminal, and you will be able to access to your RPi thru dataplicity.com


## Additional Information

1. This application is built with kivy. So please check [Official Site](https://kivy.org) for more details.

2. How is this application launched automatically?

    I added a launcher script to `/etc/rc.local`. See *line 26* of [install.sh](install.sh) for more details.
    
    As you could see in the installation script, it is **important** to clone this repo to `/home/pi/sensor-screen`
