import datetime
import os
import threading
from functools import partial
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import DictProperty
from kivy.logger import Logger
from screens.base import BaseScreen
from settings import AUTO_SAVE_INTERVAL
from utils.bme280 import read_sensor_data
from orm import SensorData
from widgets.dialog import MessageDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'home.kv'))


class HomeScreen(BaseScreen):

    _clk = None
    data = DictProperty()

    def on_enter(self, *args):
        super().on_enter(*args)
        Clock.schedule_interval(lambda dt: self.read_sensor_data(), 1)

    def read_sensor_data(self):
        self.data = read_sensor_data()
        self.ids.temperature.set_value(self.data['temp_c'])
        for k in self.data.keys():
            if k in self.ids:
                self.ids[k].set_value(self.data[k])

    def on_chk(self, chk):
        if chk.active:
            if self._clk is None:
                self._clk = Clock.schedule_interval(lambda dt: self.save_data(), AUTO_SAVE_INTERVAL)
        else:
            if self._clk:
                self._clk.cancel()
                self._clk = None

    def on_btn_ok(self):
        self.save_data()

    def on_btn_suggested_settings(self):
        threading.Thread(target=self._get_suggested_values).start()

    def _get_suggested_values(self):
        # TODO: Add math here to recommend value
        suggested_val = 5
        Clock.schedule_once(partial(self._show_suggested_dialog, suggested_val))

    @mainthread
    def _show_suggested_dialog(self, val, *args):
        MessageDialog(title='Suggested Value', label='Suggested Value: {}'.format(val)).open()

    def save_data(self):
        grind = self.ids.grind.get_value()
        quality = self.ids.quality.get_value()
        SensorData.create(temperature=self.data['temp_c'],
                          humidity=self.data['humidity'],
                          pressure=self.data['pressure'],
                          grind=grind,
                          quality=quality
                          )
        # TODO: Send data to cloud database here.
        Logger.info('Sensor Data: {} {}, Grind: {}, quality: {}'.format(
            datetime.datetime.now(), self.data, grind, quality))
