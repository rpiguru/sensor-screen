from kivy.logger import Logger
from kivy.app import App
from kivy.uix.screenmanager import Screen
import settings
from widgets.dialog import PasswordDialog


class BaseScreen(Screen):

    app = None

    def __init__(self, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)
        self.app = App.get_running_app()

    def switch_screen(self, screen_name, direction=None):
        self.app.switch_screen(screen_name, direction)

    def on_btn_exit(self):
        dlg = PasswordDialog(pwd=settings.ADMIN_PASSWORD, title='Close Application')
        dlg.bind(on_success=self.on_success_login)
        dlg.open()

    def on_success_login(self, *args):
        Logger.warning('Closing Application now...')
        self.app.stop()
