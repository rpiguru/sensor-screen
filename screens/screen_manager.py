from kivy.uix.screenmanager import ScreenManager
from screens.error_screen.screen import ErrorScreen
from screens.home_screen.screen import HomeScreen

screens = {
    'home': HomeScreen,
    'error': ErrorScreen,
}

sm = ScreenManager()
