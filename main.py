#!/usr/bin/python3
import os
import sys
import time
import gc


app_dir = os.path.abspath(os.path.dirname(__file__))
if app_dir not in sys.path:
    sys.path.append(app_dir)


from kivy.app import App
import config_kivy
import widgets.factory_reg
import traceback
from kivy.uix.screenmanager import SlideTransition, NoTransition
from kivymd.theming import ThemeManager
from screens.screen_manager import screens, sm
from kivy.logger import Logger
from kivy.base import ExceptionHandler, ExceptionManager
from utils.common import get_free_gpu_size, disable_screen_saver, is_rpi
from settings import *
from orm import SensorData

cur_dir = os.path.dirname(os.path.realpath(__file__))


class KioskExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        Logger.exception(exception)
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=20))
        _app.switch_screen('error')
        return ExceptionManager.PASS


ExceptionManager.add_handler(KioskExceptionHandler())


class SensorScreenApp(App):

    theme_cls = ThemeManager()
    current_screen = None
    exception = None

    def build(self):
        self.title = 'Sensor Screen'
        self.switch_screen(INIT_SCREEN)
        return sm

    def switch_screen(self, screen_name, direction=None, duration=.3):
        if sm.has_screen(screen_name):
            sm.current = screen_name
        else:
            s_time = time.time()
            screen = screens[screen_name](name=screen_name)
            if direction:
                sm.transition = SlideTransition(direction=direction, duration=duration)
            else:
                sm.transition = NoTransition()
            sm.switch_to(screen)
            msg = ' :: GPU - {}'.format(get_free_gpu_size()) if is_rpi() else ''
            Logger.info('SensorScreen: === Switched to {} screen {}, elapsed: {}s'.format(
                screen_name, msg, round(time.time() - s_time, 3)))
            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception


if __name__ == '__main__':

    Logger.debug('=============== Starting Sensor Screen ===============')

    if is_rpi():
        disable_screen_saver()

    if not SensorData.table_exists():
        SensorData.create_table()

    app = SensorScreenApp()

    try:
        app.run()
    except KeyboardInterrupt:
        app.stop()
