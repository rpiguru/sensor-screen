#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root"
  exit 1
fi

apt-get update

apt-get install -y python3 libpython3-dev python3-pip spidev
apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-{bad,base,good,ugly} gstreamer1.0-{omx,alsa} python-dev libmtdev-dev xclip

pip3 install -U pip
pip3 install -U Cython==0.25.2
pip3 install git+https://github.com/kivy/kivy.git@master
pip3 install wifi netifaces psutil

cd /tmp
wget abyz.me.uk/rpi/pigpio/pigpio.zip
unzip pigpio.zip
cd PIGPIO
make
sudo make install

sudo sed -i -- "s/^exit 0/pigpiod \&\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/(cd \/home\/pi\/sensor-screen \&\& python3 main.py)\&\\nexit 0/g" /etc/rc.local

sudo echo "dtparam=spi=on" | sudo tee -a /boot/config.txt
sudo echo "gpu_mem=512" | sudo tee -a /boot/config.txt

sudo apt-get install -y sqlite3
sudo pip3 install peewee
