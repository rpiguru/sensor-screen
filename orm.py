import datetime
import os
from peewee import *
from settings import SQLITE_FILE

cur_dir = os.path.abspath(os.path.dirname(__file__))

db = SqliteDatabase(os.path.join(cur_dir, SQLITE_FILE))
db.connect()


class SensorData(Model):

    datetime = DateTimeField(default=datetime.datetime.now)
    temperature = FloatField(null=True)
    humidity = FloatField(null=True)
    pressure = FloatField(null=True)
    grind = IntegerField(null=True)
    quality = IntegerField(null=True)

    class Meta:
        database = db
